<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 12/29/14
 * Time: 1:34 PM
 */

namespace ScoutingOla\Presenters;

use Nette,
    Nette\Application\UI,
    Nette\ArrayHash,
    ScoutingOla\Security\Passwords;
use ScoutingOla\Components\EditMemberControl;


class MembersPresenter extends BasePresenter
{
    /** @var  \ScoutingOla\Model\UserRepository @inject*/
    public $userRepository;

    /** @var  \Nette\Database\Table\ActiveRow */
    protected $group;

    public function startup()
    {
        parent::startup();
        $this->group = $this->groupRepository->getGroupById($this->user->getIdentity()->group_id);
    }

    public function renderDefault()
    {
        $this['breadCrumb']->addLink('Members', FALSE, 'fa-group');

        $numberOfMembers = $this->groupRepository->getNumberOfMembers($this->group->id);

        $this->template->members = $this->userRepository->getAllGroupMembers($this->group->id);
        $this->template->numberOfMembers = $numberOfMembers;
        $this->template->filledUpRatio = round(($numberOfMembers / $this->group->scout_limit)*100);
    }

    protected function createComponentEditMember ()
    {
        $repository = $this->userRepository;

        $control = new UI\Multiplier(function ($name) use ($repository){
            $control = new EditMemberControl($name, $repository);
            return $control;
        });

        return $control;
    }

    protected function createComponentAddMember ()
    {
        $form = new UI\Form();
        $form->addText('name', 'Name:')->setRequired();
        $form->addText('email', 'Email:');
        $form->addPassword('password', 'Password:')->setRequired();
        $form->addSubmit('add', 'Add New Member');

        $form->onSuccess[] = $this->addMemberSuccess;

        return $form;
    }

    public function addMemberSuccess(UI\Form $form)
    {
        $values = $form->getValues();

        $values = ArrayHash::from(array(
            'name' => $values->name,
            'email' => $values->email,
            'password' => $values->password,
            'group_id' => $this->group->id,
            'role' => "scout"
        ));

        try{
            $this->userRepository->newUser($values);
            $this->flashMessage("User {$values->name} was added", 'success');
            $this->refresh();
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage($e->getMessage(), 'danger');
            $this->refresh();
        }
    }
} 