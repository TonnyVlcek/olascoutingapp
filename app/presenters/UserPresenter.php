<?php

namespace ScoutingOla\Presenters;


use Nette\Application\UI\Form,
    Nette\Security\AuthenticationException,
    Nette\DI\Extensions;

class UserPresenter extends BasePresenter
{
    /** @var \ScoutingOla\Model\UserRepository @inject */
    public $userRepository;

    public function actionLogin()
    {
        if($this->user->isLoggedIn()){
            $this->redirect('Homepage:');
        }
    }

    protected function createComponentLoginForm ()
    {
        $form = new Form;
        $form->addText('group_name', 'Group Name')->setRequired();
        $form->addText('user_name', 'User Name:')->setRequired();
        $form->addPassword('password', 'Password:')->setRequired();
        $form->addSubmit('login', 'Sign IN');
        $form->onSuccess[] = $this->loginFormSuccess;

        return $form;
    }

    public function loginFormSuccess (Form $form){
        $values = $form->getValues();
        try{
            $this->user->login($values->user_name, $values->password, $values->group_name);
            $this->flashMessage('Login successful.', 'success');
            $this->redirect('Homepage:');
        } catch (AuthenticationException $e){
            $this->flashMessage($e->getMessage(), 'danger');
            $this->refresh();
        }
    }

    public function renderProfile($id)
    {
        $this->template->id = $id;
    }

    /*public function actionVerification($id, $verificationKey)
   {
       $userValues = $this->userRepository->getUserById($id);

       if(!$userValues){
           $this->flashMessage('This user doesn\'t exist');
           $this->redirect('Homepage:default');
       }

       try {
           $this->userRepository->verifyUser($id, $verificationKey);
       } catch (AuthenticationException $e) {
           $this->flashMessage($e->getMessage(), 'error');
           $this->redirect('Homepage:default');
       }

       $this->flashMessage('Verification successful');
       $this->redirect("User:login");
   }*/
}
