<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 12/29/14
 * Time: 6:55 PM
 */

namespace ScoutingOla\Presenters;


use Nette\Application\UI\Form;
use Nette\Diagnostics\Debugger;

class CompetitionPresenter extends BasePresenter
{
    /** @var  \ScoutingOla\Model\CompetitionRepository @inject */
    public $competitionRepository;

    public function renderDefault()
    {
        $this['breadCrumb']->addLink('Competitions', FALSE, 'fa-trophy');

        $this->template->competitions = $this->competitionRepository->getGroupCompetitions($this->user->getIdentity()->group_id);
    }

    protected function createComponentCompetitionForm ()
    {
        $form = $this->createForm();
        $form->addText('name', 'Name:')->setRequired();
        $form->addText('date', 'Date:')->setRequired();
        $form->addTextArea('description', 'Description:');
        $form->addText('location', 'Location:');
        $form->addTextArea('teams', 'Teams:');
        return $form;
    }

    protected function createComponentAddCompetitionForm ()
    {
        $form = $this->createComponentCompetitionForm();
        $form->addSubmit('add', 'Add');
        $form->onSuccess[] = $this->addCompetitionSuccess;

        return $form;
    }

    protected function createComponentEditCompetitionForm ()
    {
        $form = $this->createComponentCompetitionForm();
        $form->addSubmit('edit', 'Save Changes');
        $form->onSuccess[] = $this->editCompetitionSuccess;

        return $form;
    }


    public function addCompetitionSuccess(Form $form)
    {
        $values = $form->getValues();
        $values->group_id = $this->user->getIdentity()->group_id;
        try{
            $this->competitionRepository->createCompetition($values);
        } catch (\Exception $e) {
            $this->flashMessage($e->getMessage(), 'danger');
            $this->refresh();
        }

        $this->flashMessage('New competition added successfully', 'success');
        $this->refresh();
    }

    public function editCompetitionSuccess(Form $form)
    {
        $values = $form->getValues();
        $values->group_id = $this->user->getIdentity()->group_id;
        /*try{
            $this->competitionRepository->createCompetition($values);
        } catch (\Exception $e) {
            $this->flashMessage($e->getMessage(), 'danger');
            $this->refresh();
        }*/

        Debugger::barDump($values);

        $this->flashMessage("Changes have been saved", 'success');
        $this->refresh();
    }

    public function handleEditCompetition($competition_id)
    {
        $competition = $this->competitionRepository->getCompetitionById($competition_id);
        $defaults = array(
            'name' => $competition->name,
            'date' => $competition->date,
            'description' => $competition->description,
            'location' => $competition->location
        );
        Debugger::barDump($defaults);

        $this->getComponent('editCompetitionForm')->setDefaults($defaults);
    }
} 