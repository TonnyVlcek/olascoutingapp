<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 2/16/15
 * Time: 4:31 PM
 */

namespace ScoutingOla\Presenters;

use Nette;
use Nette\Application\UI\Form;


class RecordPresenter extends BasePresenter
{
    /** @var  \ScoutingOla\Model\CompetitionRepository @inject*/
    public $compRepository;

    /** @var  \ScoutingOla\Model\TeamsRepository @inject*/
    public $teamsRepository;

    /** @var  \ScoutingOla\Model\TagRepository @inject*/
    public $tagRepository;

    /** @var  \ScoutingOla\Model\RecordRepository @inject*/
    public $recordRepository;

    /** @var  \ScoutingOla\Model\RecordTagRepository @inject*/
    public $recordTagRepository;


    /** @var   Nette\Database\Table\IRow*/
    protected $group;

    /** @var  Nette\Database\Table\IRow */
    public $competition;

    private $session;


    /**
     * Sets up protected parameter $group_id
     */
    public function startup()
    {
        $this->loginRequired();
        $this->group = $this->groupRepository->getGroupById($this->user->getIdentity()->group_id);
        $this->session = $this->getSession()->getSection('Record');
        parent::startup();
    }

    public function renderDefault()
    {
        if(isset($this->session->selected_competition)){
            $this->redirect('Record:new', $this->session->selected_competition);
        }
        $this->template->competitions = $this->compRepository->getCompetitionPairs($this->group->id);
    }

    protected function createComponentSelectCompetition()
    {
        $form = $this->createForm();
        $form->addSelect('competition', 'Select Competition', $this->compRepository->getCompetitionPairs($this->group->id));
        $form->addSubmit('select', 'Select');
        $form->onSuccess[] = $this->selectCompetitionSuccess;

        return $form;
    }

    public function selectCompetitionSuccess(Form $form)
    {
        $competition_id = $form->getValues()->competition;
        $this->session->selected_competition = $competition_id; //TODO: transfer this to model layer
        $this->flashMessage('Competition selected', 'success');
        $this->redirect('Record:new', $competition_id);
    }

    public function handleCompetitionLogout()
    {
        unset($this->session->selected_competition);
        $this->flashMessage('Select new competition');
        $this->redirect('Record:');
    }

    /**
     * Sets up protected parameter $competition_id and verifies is user can use this site
     *
     * @param $id
     * @throws \Nette\Application\ForbiddenRequestException
     * @throws \Nette\Application\BadRequestException
     */
    public function actionNew($id)
    {
        if(!$this->user->isAllowed('record', 'add')){
            throw new Nette\Application\ForbiddenRequestException;
        }
        if(!$this->competition = $this->compRepository->getCompetitionById($id)){
            throw new Nette\Application\BadRequestException;
        }

        $this->template->group = $this->group;
        $this->template->competition = $this->competition;
    }

    public function renderDone($id)
    {
        if(!$record = $this->recordRepository->getRecordById($id)){
            throw new Nette\Application\BadRequestException;
        }
        $this->template->record = $record;
        $this->template->relatedTags = $this->recordTagRepository->getRelatedTags($record->id);


    }

    protected function createComponentAddRecord ()
    {
        $form = $this->createForm(); //TODO: how to work with required?
        $form->addText('match_number', 'Match Number')
            ->addRule(Form::INTEGER, '%label Must be numeric')
            ->setRequired();
        $form->addSelect('team_id', 'Select Team', $this->teamsRepository->getTeamsPairs($this->competition->id))
            ->setRequired();
        $form->addRadioList('ally_color', 'Ally Color', array('red' => 'Red', 'blue' => 'Blue'))
            ->setRequired();
        $form->addRadioList('autonomous', 'Autonomous', array('yes' => 'Yes', 'no' => 'No'))
            ->setRequired();
        $form->addText('autonomous_points', 'Autonomous Points')
            ->addRule(Form::INTEGER, '%label Must be numeric');
        $form->addText('points_one', 'Skyrise')
            ->addRule(Form::INTEGER, '%label Must be numeric');
        $form->addText('points_two', 'Cubes')
            ->addRule(Form::INTEGER, '%label Must be numeric');
        $form->addRadioList('match_result', 'Match Result', array('win' => 'Win', 'loss' => 'Loss'))
            ->setRequired();
        $form->addMultiSelect('tags', 'Tags', $this->tagRepository->getTagsPairs($this->group->id));
        $form->addText('rating', 'Overall rating')
            ->addRule(Form::INTEGER, '%label Must be numeric');

        $form->addHidden('competition_id', $this->request->getParameters()['id']);
        $form->addSubmit('add_match', 'Add match');
        $form->onSuccess[] = $this->addRecordSuccess;

        return $form;
    }

    public function addRecordSuccess (Form $form){
        $values = $form->getValues();
        $tags = $values->tags;
        unset($values->tags);

        $values->user_id = $this->user->id;
        $values->group_id = $this->group->id;

        $new_record = $this->recordRepository->addRecord($values);

        if(count($tags) !== 0) {
            $this->recordTagRepository->addConnection($new_record->id, $tags);
        }

        $this->flashMessage('Success', 'success');
        $this->redirect('Record:done', $new_record->id);
    }
}
