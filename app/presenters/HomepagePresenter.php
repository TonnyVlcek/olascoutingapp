<?php

namespace ScoutingOla\Presenters;

use Nette,
	ScoutingOla\Model,
    Nette\Application\UI;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
    /** @var  \ScoutingOla\Model\CompetitionRepository @inject*/
    public $competitionRepository;

    /** @var  \ScoutingOla\Model\RecordRepository @inject*/
    public $recordRepository;

    /** @var  \ScoutingOla\Model\UserRepository @inject*/
    public $userRepository;

    /** @var  \ScoutingOla\Model\GroupRepository @inject*/
    public $groupRepository;

    private $group;

    public function actionDefault()
    {
        $this->loginRequired();
        $this->group = $this->groupRepository->getGroupById($this->user->getIdentity()->group_id);
        $this->setView($this->user->getIdentity()->role);
    }

    public function renderScout()
    {
        $this->template->scout = $this->userRepository->getUserById($this->user->id);
    }

    public function renderAdmin()
    {
        $this->template->widgetData = array(
            'competitions' => count($this->competitionRepository->getGroupCompetitions($this->group->id)),
            'records' => count($this->recordRepository->getRecords($this->group->id)),
            'members' => count($this->userRepository->getAllGroupMembers($this->group->id))
        );
    }

    public function renderOwner()
    {

    }
}
