<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 2/23/15
 * Time: 9:21 PM
 */

namespace ScoutingOla\Presenters;

use ScoutingOla\Components\Grido as Grido;
use Nette;
use Nette\Utils\Html;

class ResultsPresenter extends BasePresenter
{
    /** @var \ScoutingOla\Model\RecordRepository @inject*/
    public $recordRepository;

    /** @var \ScoutingOla\Model\RecordTagRepository @inject*/
    public $recordTagRepository;

    /** @var \ScoutingOla\Model\TagRepository @inject*/
    public $tagRepository;

    /** @var \ScoutingOla\Model\CompetitionRepository @inject*/
    public $competitionRepository;


    /** @var Nette\Database\Table\IRow*/
    private $competition;

    /** @var Nette\Database\Table\IRow*/
    private $tag;


    public function startup()
    {
        $this->loginRequired();
        if(!$this->user->isAllowed('results', 'view')){
            throw new Nette\Application\ForbiddenRequestException;
        }
        parent::startup();
    }

    public function actionDefault($competition, $tag)
    {
        //If competition doesn't exist
        /*if((isset($competition))&&(!$this->competition = $this->competitionRepository->getCompetitionById($competition))); {
            throw new Nette\Application\BadRequestException;
        }*/

        $this['breadCrumb']->addLink('Records');
        //$this->competition = $this->competitionRepository->getCompetitionById($competition);
        //$this->tag = $this->tagRepository->getTagById($tag);
    }

    public function renderDefault()
    {
        $data = $this->recordRepository->getRecords($this->user->getIdentity()->group_id);
        $this->template->data = $data;
    }

    public function handleDeleteRecord($record_id)
    {
        $this->recordRepository->deleteRecord($record_id);
        $this->flashMessage("Record #{$record_id} was deleted", 'success');
        $this->refresh();
    }
}
