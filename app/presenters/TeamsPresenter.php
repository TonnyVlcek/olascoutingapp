<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 1/1/15
 * Time: 7:02 PM
 */

namespace ScoutingOla\Presenters;

use Nette,
    Nette\Application\UI;

class TeamsPresenter extends BasePresenter
{
    /** @var \ScoutingOla\Model\TeamsRepository @inject */
    public $teamsRepository;

    public function renderDefault()
    {
        $this['breadCrumb']->addLink('Teams');
        $this->template->publicTeams = $this->teamsRepository->getPublicTeams();
        $this->template->privateTeams = $this->teamsRepository->getPrivateTeams($this->user->getIdentity()->group_id);
    }

    protected function createComponentAddTeam ()
    {
        $form = $this->createForm();
        $form->addText('number', 'Number:', 4, 5)->setRequired();
        $form->addText('letter', 'Letter:', 1, 1)->setRequired();
        //TODO: add more inforamtion
        $form->addSubmit('add', 'Add team');
        $form->onSuccess[] = $this->addTeamSuccess;

        return $form;
    }

    public function addTeamSuccess(UI\Form $form)
    {
        $values = $form->getValues();

        $values->group_id = $this->user->getIdentity()->group_id;
        try{
            $this->teamsRepository->createTeam($values);
            $this->flashMessage("Team {$values->number}{$values->letter} was added", 'success');
            $this->refresh();
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage($e->getMessage(), 'danger');
            $this->refresh();
        }
    }
} 