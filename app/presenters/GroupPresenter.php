<?php

namespace ScoutingOla\Presenters;


use Nette;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\DI\Extensions;
use Nette\ArrayHash;

class GroupPresenter extends BasePresenter
{
    /** @var \ScoutingOla\Model\UserRepository @inject */
    public $userRepository;

    /** @var  \ScoutingOla\Model\GroupRepository @inject*/
    public $groupRepository;

    /** @var  \ScoutingOla\ConfigManager\Settings @inject*/
    public $settings;

    /** @var  \ScoutingOla\Emails\EmailSender @inject */ //TODO: start sending emails
    public $mailer;


    public function actionCreate()
    {
        if($this->user->isLoggedIn()){
            $this->redirect('Homepage:');
        }
    }

    protected function createComponentCreateGroupForm ()
    {
        $form = $this->createForm();
        //info about admin
        $form->addGroup('Admin Info');
            $form->addText('admin_name', 'Admin Name:', NULL, 50)
                ->setRequired('Please fill in your name');
            $form->addText('email', 'Admin Email:', NULL, 50)
                ->setType('email')
                ->setRequired('Please fill in your email address')
                ->addRule($form::EMAIL, 'Please check for typos in the email address');
            $form->addPassword('password', 'Password:')
                ->setRequired('Please fill in your password')
                ->addRule($form::MIN_LENGTH, 'Password has to be at least %s charts long', 5);
            $form->addPassword('password_check', 'Password again:')
                ->setRequired('Please fill in control password (Password again)')
                ->addRule($form::EQUAL, 'Please check typos, passwords aren\'t the same.', $form['password']);
        //Info about new group
        $form->addGroup('Group Info');
            $form->addText('group_name', 'Group Name', NULL, 50)
               ->setRequired('Please select Group Name');
            $form->addTextArea('group_description', 'Group Description');
        //Send button
        $form->addSubmit('create', 'Create Group');
        $form->onSuccess[] = $this->createGroupFormSuccess;

        return $form;
    }

    public function createGroupFormSuccess(Form $form){
        $values = $form->getValues();

        //TODO: change services in model
        //set up group data
        $group_values = ArrayHash::from(array(
            'name' => $values->group_name,
            'description' => $values->group_description,
            'scout_limit' => $this->settings->__get("default_scout_limit")
        ));

        //create new group

        try {
            $new_group = $this->groupRepository->createGroup($group_values);
        } catch (AuthenticationException $e){
            $this->flashMessage(sprintf($e->getMessage(), $values->group_name), "danger");
            $this->refresh();
            exit;
        }

        $admin_values = ArrayHash::from(array(
            'name' => $values->admin_name,
            'email' => $values->email,
            'password' => $values->password,
            'group_id' => $new_group,
            'role' => "admin"
        ));
        //TODO: if there is an exception -> group gets created but there is no way how to get in
        //create new admin
        try {
            $this->userRepository->newUser($admin_values);
        } catch (AuthenticationException $e){
            $new_group->delete();
            $this->flashMessage(sprintf($e->getMessage(), $values->group_name), "danger");
            $this->refresh();
        }


        //TODO: send an email

        //flashMessage and redirect to homepage
        $this->flashMessage("Group {$values->group_name} was successfully created", "success");
        $this->redirect("User:login");
    }

    public function actionEdit()
    {
        $this->loginRequired();
        if(!$this->user->isAllowed('group', 'edit')){
            throw new Nette\Application\ForbiddenRequestException;
        }

        $this['breadCrumb']->addLink('Group');
        $this['breadCrumb']->addLink('Profile');

    }

    protected function createComponentGroupEdit ()
    {
        $group = $this->groupRepository->getGroupById($this->user->getIdentity()->group_id);
        $form = $this->createForm();
        $form->addText('name', 'Name')->setRequired();
        $form->addTextArea('description', 'Description');
        $form->addSubmit('edit', 'Save');
        $form->addHidden('group_id', $group->id);
        $form->setDefaults(array(
            'name' => $group->name,
            'description' => $group->description
        ));
        $form->onSuccess[] = $this->groupEditSuccess;

        return $form;
    }

    public function groupEditSuccess(Form $form)
    {
        $values = $form->getValues();
        try{
            $this->groupRepository->editGroup($values->group_id, $values->name, $values->description);
            $this->flashMessage('Group information updated', "success");
            $this->refresh();
        } catch (AuthenticationException $e) {
            $this->flashMessage(sprintf($e->getMessage(), $values->name), "danger");
            $this->refresh();
        }
    }
}
