<?php

namespace ScoutingOla\Presenters;

use Nette,
	ScoutingOla\Model,
    Nette\DI\Extensions,
    Nette\Application\UI\Form;
use Nette\ComponentModel\Component;
use ScoutingOla\Components\BreadCrumbControl;
use ScoutingOla\Components\FlashControl;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var  \ScoutingOla\Model\GroupRepository @inject*/
    public $groupRepository;

    public function __construct()
    {

    }

    public function startup() {
        parent::startup();
    }

    public function beforeRender()
    {
        if($this->user->isLoggedIn()){
            $this->template->group = $this->groupRepository->getGroupById($this->user->getIdentity()->group_id);
        }
    }
    public function createForm()
    {
        $form = new Form;
        return $form;
    }

    protected function createComponentFlash ()
    {
        $control = new FlashControl($this->template->flashes);
        return $control;
    }

    protected function createComponentBreadCrumb ()
    {
        $breadCrumb = new BreadCrumbControl();
        $breadCrumb->addLink('Main page', $this->link('Homepage:'), 'fa-home');

        return $breadCrumb;
    }


    /**
     * @return void
     */
    public function refresh ()
    {
        $this->redirect('this');
    }

    public function loginRequired()
    {
        if(!$this->user->isLoggedIn()) {
            $this->flashMessage('You have to be logged in to access this site.', 'danger');
            $this->redirect('User:login'); //TODO: after login redirect to the right page
        }
    }

    public function handleLogout()
    {
        $this->user->logout(); // user logout
        $this->flashMessage('Logout was successful', 'success');
        $this->redirect('User:login');
    }

}
