<?php

namespace ScoutingOla\Presenters;


use Nette;
use Nette\Application\UI;

class TagsPresenter extends BasePresenter
{
    /** @var \ScoutingOla\Model\TagRepository @inject */
    public $tagRepository;

    /** @var \ScoutingOla\Model\RecordTagRepository @inject */
    public $recordTagRepository;

    private $tags;
    private $group;

    public function startup()
    {
        $this->loginRequired();
        if(!$this->user->isAllowed('tags', 'view')){
            throw new Nette\Application\ForbiddenRequestException;
        }

        $this->group = $this->groupRepository->getGroupById($this->user->getIdentity()->group_id);
        parent::startup();
    }

    public function actionDefault()
    {
        $this['breadCrumb']->addLink('Settings', FALSE, 'fa-cogs');
        $this['breadCrumb']->addLink('Tags', FALSE, 'fa-tags');


        $this->tags = $this->tagRepository->getTags($this->group->id);
    }

    public function renderDefault()
    {
        $this->template->tags = $this->tags;
    }

    protected function createComponentAddTag ()
    {
        $form = $this->createForm();
        $form->addText('name', 'Name:', NULL, 255)->setRequired();
        $form->addSubmit('add', 'Add Tag');
        $form->onSuccess[] = $this->addTagSuccess;

        return $form;
    }

    public function addTagSuccess(UI\Form $form)
    {
        $values = $form->getValues();

        $values->group_id = $this->group->id;

        try{
            $this->tagRepository->createTag($values);
            $this->flashMessage("Tag {$values->name} was added", 'success');
            $this->refresh();
        } catch (\Exception $e) {
            $this->flashMessage($e->getMessage(), 'danger');
            $this->refresh();
        }
    }

    public function handleEditTag($tag_id)
    {

    }

    public function handleDeleteTag($tag_id)
    {

    }

}
