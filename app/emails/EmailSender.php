<?php

namespace ScoutingOla\Emails;


use Nette\Diagnostics\Debugger,
    Nette\Object,
    Nette\Mail\SendmailMailer,
    Nette\Mail\Message,
    Nette\Templating\FileTemplate,
    Nette\Latte\Engine;

class EmailSender extends Object
{

    public function sendVerificationMail($toWhom, $verificationCode)
    {
        $template = $this->createTemplate(__DIR__."/verificationEmail.latte");
        $template->verificationCode = $verificationCode;

        $mail = new Message;
        $mail->setFrom('') //Admin Email
            ->addTo($toWhom)
            ->setHtmlBody($template);

        $mailer = new SendmailMailer;
       // $mailer->send($mail);

        Debugger::barDump($mail);
    }

    public function createTemplate($templateName)
    {
        $template = new FileTemplate($templateName);
        $template->registerFilter(new Engine);
        $template->registerHelperLoader('Nette\Templating\Helpers::loader');

        return $template;
    }
} 