<?php

namespace ScoutingOla;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();
        $router[] = new Route("<presenter>/<action>[/<id>]", array(
            "presenter" => "Landing",
            "action" => "default",
        ));

		return $router;
	}

}
