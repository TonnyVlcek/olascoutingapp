<?php

namespace ScoutingOla\Security;

use Nette\Diagnostics\Debugger;
use ScoutingOla\Model\GroupRepository;
use ScoutingOla\Model\UserRepository,
    ScoutingOla\Security\Passwords,
    Nette\Object,
    Nette\Security\AuthenticationException,
    Nette\Security\IAuthenticator,
    Nette\Security\Identity;

class Authenticator extends Object implements IAuthenticator
{
    protected $userRepository;
    protected $groupRepository;
    
    public function __construct(UserRepository $userRepository, GroupRepository $groupRepository)
    {
        $this->userRepository = $userRepository;
        $this->groupRepository = $groupRepository;
    }
    
    public function authenticate (array $credentials)
    {
        list($user_name, $password, $group_name) = $credentials;

        if(!$group = $this->groupRepository->getGroupByName($group_name)){
            throw new AuthenticationException("Group {$group_name} not found.");
        }
        if(!$user = $this->userRepository->userInGroup($group->id, $user_name)){
            throw new AuthenticationException("User {$user_name} not found in this group.");
        }
        if(md5($password) !== $user->password){
            throw new AuthenticationException("Invalid password.");
        }

        return new Identity($user->id, $user->role, $user);
    }
}
