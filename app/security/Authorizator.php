<?php

namespace ScoutingOla\Security;


use Nette\Security\Permission;

class Authorizator extends  Permission{
    public function __construct ()
    {
        $this->addRole('guest');
        $this->addRole('scout', 'guest');
        $this->addRole('admin', 'scout');
        $this->addRole('owner', 'admin');

        $this->addResource('results');
        $this->addResource('record');
        $this->addResource('group');
        $this->addResource('tags');
        $this->addResource('administration');

        $this->allow('scout', array('results', 'tags'), 'view');
        $this->allow('scout', 'record', 'add');

        $this->allow('admin', array('group', 'administration'), 'view');
        $this->allow('admin', array('group'), 'edit');
        $this->allow('admin', array('results'), 'manage');

        $this->allow('owner');
    }
}
