<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 12/29/14
 * Time: 12:03 AM
 */

namespace ScoutingOla\Components;

use Nette\Application\UI;

class FlashControl extends UI\Control
{

    protected $flashes;

    public function __construct($flashes)
    {
        $this->flashes = $flashes;
    }
    public function render()
    {
        $this->template->setFile(__DIR__ . '/FlashControl.latte');
        $this->template->flashes = $this->flashes;
        $this->template->render();
    }

} 