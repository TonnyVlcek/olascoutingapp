<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 12/29/14
 * Time: 3:33 PM
 */

namespace ScoutingOla\Components;

use Nette\Application\UI;
use Nette\Diagnostics\Debugger;
use Nette\Security\AuthenticationException;
use ScoutingOla\Model\UserRepository;

class EditMemberControl extends UI\Control
{
    /** @var  \ScoutingOla\Model\UserRepository */
    public $userRepository;

    /** @var \Nette\Database\Table\ActiveRow*/
    private $member;

    public function __construct($member_id, $userRepository = NULL)
    {
        $this->userRepository = $userRepository;
        $this->member = $this->userRepository->getUserById($member_id);
    }
    public function render()
    {
        $this->template->setFile(__DIR__ . '/EditMemberControl.latte');
        $this->template->member = $this->member;
        $this->template->render();
    }

    protected function createComponentSingleEdit ()
    {
        $form = new UI\Form();
        $form->addText('name', 'Name:');
        $form->addText('email', 'Email:');

        $form->addSubmit('save', 'Save');

        $form->setDefaults(array(
            'name' => $this->member->name,
            'email' => $this->member->email
        ));

        $form->onSuccess[] = $this->singleEditSuccess;

        return $form;
    }

    public function singleEditSuccess(UI\Form $form)
    {
        $values = $form->getValues();

        try {
            $this->userRepository->editUser($this->member->id, $values);
        } catch (AuthenticationException $e) {
            $this->flashMessage($e->getMessage());
            $this->redirect('this');
        }

        $this->presenter->flashMessage('User edited', 'success');
        $this->redirect('this');

    }

    public function handleDeleteUser($user_id)
    {
        try {
            $this->userRepository->deleteUser($user_id);
        } catch (\Exception $e) {
            $this->presenter->flashMessage("Danger: {$e->getMessage()}", 'danger');
        }

        $this->presenter->flashMessage('User deleted', 'success');
        $this->redirect('this');
    }
} 