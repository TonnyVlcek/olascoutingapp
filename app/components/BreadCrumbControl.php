<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 12/29/14
 * Time: 11:54 AM
 */

namespace ScoutingOla\Components;

use Nette\Application\UI;

class BreadCrumbControl extends UI\Control
{
    /** @var array links */
    public $links = array();

    /**
     * Render function
     */
    public function render()
    {
        $this->template->setFile(__DIR__ . '/BreadCrumbControl.latte');
        $this->template->links = $this->links;
        $this->template->render();
    }
    /**
     * Add link
     *
     * @param $title
     * @param Nette/Application/UI/Links $link
     * @param null $icon
     */
    public function addLink($title, $link = NULL, $icon = NULL)
    {
        $this->links[] = array(
            'title' => $title,
            'link'  => $link,
            'icon'  => $icon
        );
    }
    /**
     * Remove link
     *
     * @param $key
     *
     * @throws Exception
     */
    public function removeLink($key)
    {
        if (is_int($key)) {
            unset($this->links[$key]);
        } else {
            throw new Exception("Key must be int.");
        }
    }
} 