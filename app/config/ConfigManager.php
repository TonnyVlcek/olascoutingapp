<?php
/**
 * @author Tony Vlcek
 * @date 12/15/2014
 */

namespace ScoutingOla\ConfigManager;

use Nette;

class Settings extends Nette\Object
{
    protected $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function &__get($name)
    {
        if (array_key_exists($name, $this->params)){
            return $this->params[$name];
        }
        else {
            trigger_error("{$name} is not in parameters Array");
        }
    }
}
