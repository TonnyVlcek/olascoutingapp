<?php
namespace ScoutingOla\Model;

use Nette;

/**
 * Class TagRepository
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class TagRepository extends BaseRepository
{
    /**
     * @param $tag_id
     * @return Nette\Database\Table\Selection
     */
    public function getTagById($tag_id)
    {
        return $this->getTable()->wherePrimary($tag_id);
    }


    /**
     * @param $group_id
     * @return Nette\Database\Table\Selection
     */
    public function getTags($group_id)
    {
        return $this->getTable()->where('group_id', $group_id)->order('name');
    }

    /**
     * @param $group_id
     * @return array
     */
    public function getTagsPairs($group_id)
    {
        return $this->getTags($group_id)->fetchPairs('id', 'name');
    }

    /**
     * Creates new tag.
     * Checks for duplicities.
     *
     * @param Nette\ArrayHash $values
     * @return bool|int|Nette\Database\Table\IRow
     * @throws \Exception
     */
    public function createTag(Nette\ArrayHash $values)
    {
        if($this->getTable()->where('group_id', $values->group_id)->where('name', $values->name)->fetch()){
            throw new \Exception("Tag {$values->name} already exists.");
        }

        return $this->getTable()->insert($values);
    }

    /**
     * @param $tag_id
     * @todo: solve for dependencies
     */
    public function deleteTag($tag_id)
    {
        $this->getTagById($tag_id)->delete();
    }
}
