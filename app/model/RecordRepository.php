<?php
/**
 * Created by PhpStorm.
 * User: Tonny
 * Date: 2/21/15
 * Time: 1:38 AM
 */

namespace ScoutingOla\Model;

use Nette;

/**
 * Class RecordRepository
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class RecordRepository extends BaseRepository
{

    /**
     * Gets all records matching the parameters
     *
     * @param $group_id
     * @return Nette\Database\Table\Selection
     */
    public function getRecords($group_id) //TODO: add filters: $competition_id = NULL, $tag_id = NULL, $user_id = NULL
    {
        $result = $this->getTable()->where('group_id', $group_id);

        return $result;
    }

    /**
     * @param $record_id
     * @return bool|mixed|Nette\Database\Table\IRow
     */
    public function getRecordById($record_id)
    {
        return $this->getTable()->wherePrimary($record_id)->fetch();
    }

    /**
     * @param $values
     * @return bool|int|Nette\Database\Table\IRow
     */
    public function addRecord($values)
    {
        return $this->getTable()->insert($values);
    }

    /**
     * @param $record_id
     */
    public function deleteRecord($record_id)
    {
        $this->getRecordById($record_id)->delete();
    }
}
