<?php
namespace ScoutingOla\Model;

use Nette;

/**
 * Class CompetitionRepository
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class CompetitionRepository extends BaseRepository
{

    /** @var  \ScoutingOla\Model\TeamsRepository */
    protected $teamsRepository;

    /** @var  \ScoutingOla\Model\CompetitionTeamRepository */
    protected $competitionTeamRepository;

    /**
     * Constructor
     *
     * @param string $table
     * @param Nette\Database\Context $context
     * @param TeamsRepository $teamsRepository
     * @param CompetitionTeamRepository $competitionTeamRepository
     */
    public function __construct($table, Nette\Database\Context $context, TeamsRepository $teamsRepository, CompetitionTeamRepository $competitionTeamRepository)
    {
        parent::__construct($table, $context);
        $this->teamsRepository = $teamsRepository;
        $this->competitionTeamRepository = $competitionTeamRepository;
    }

    /**
     * @param $competition_id
     * @return bool|mixed|Nette\Database\Table\IRow
     */
    public function getCompetitionById($competition_id)
    {
        return $this->getTable()->wherePrimary($competition_id)->fetch();
    }

    /**
     * Selects all competitions created with the group
     *
     * @param $group_id
     * @return Nette\Database\Table\Selection
     */
    public function getGroupCompetitions($group_id)
    {
        return $this->getTable()->where('group_id', $group_id);
    }

    public function getCompetitionPairs($group_id)
    {
        return $this->getGroupCompetitions($group_id)->fetchPairs('id', 'name');
    }

    /**
     * Parse team names from a string
     *
     * @param $teamsString
     * @return array Team Names
     * @throws \Exception
     */
    public function teamStringRecognition($teamsString)
    {
        $teams = array();
        if(!preg_match_all('/(\\d+)\\s*(\\w)?\\s*,?/', $teamsString, $teams, PREG_PATTERN_ORDER)){
            throw new \Exception('No team name found in this string');
        }

        return $teams;
    }

    /**
     * Creates new competitions and adds connections to teams related to this competition
     *
     * @param Nette\ArrayHash $values
     * @return Nette\Database\Table\IRow
     * @throws \Exception
     */
    public function createCompetition(Nette\ArrayHash $values)
    {
        $teamNames = $this->teamStringRecognition($values->teams);
        unset($values->teams);

        //getting list of teamIds form their names
        $teamIds = array();
        for($counter = 0; $counter < sizeof($teamNames[1]); $counter++){
            $team_number = $teamNames[1][$counter];
            $team_letter = $teamNames[2][$counter];

            if($team = $this->teamsRepository->getTeamByName($team_number, $team_letter)){
                $teamIds[] = $team->id;
            } else {
                throw new \Exception("Team {$team_letter}{$team_number} wasn't found.");
            }
        }
        //If everything is alright save data to database
        $competition = $this->getTable()->insert($values);
        $this->competitionTeamRepository->addConnection($competition->id, $teamIds);

        return $competition;
    }
} 
