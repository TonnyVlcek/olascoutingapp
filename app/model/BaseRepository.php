<?php
namespace ScoutingOla\Model;

use Nette\Database\Context;
use Nette\Object;

/**
 * Class BaseRepository
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class BaseRepository extends Object
{
    /** @var \Nette\Database\Context */
    protected $context;

    /** @var string */
    protected $table;

    /**
     * @param Context $context
     * @param string $table
     */
    function __construct($table, Context $context)
    {
        $this->context = $context;
        $this->table = $table;
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getTable ()
    {
        return $this->context->table($this->table);
    }
}
