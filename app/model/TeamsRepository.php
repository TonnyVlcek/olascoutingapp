<?php
namespace ScoutingOla\Model;

use ScoutingOla\ConfigManager;
use Nette;
use Nette\ArrayHash;
use Nette\InvalidArgumentException;

/**
 * Class TeamsRepository
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class TeamsRepository extends BaseRepository
{

    /** @var \ScoutingOla\ConfigManager\Settings  */
    public $settings;

    /** @var \ScoutingOla\Model\CompetitionTeamRepository  */
    public $competitionTeamRepository;

    /**
     * @param string $table
     * @param Nette\Database\Context $context
     * @param ConfigManager\Settings $settings
     * @param CompetitionTeamRepository $competitionTeamRepository
     */
    public function __construct($table, Nette\Database\Context $context, ConfigManager\Settings $settings, CompetitionTeamRepository $competitionTeamRepository)
    {
        parent::__construct($table, $context);
        $this->settings = $settings;
        $this->competitionTeamRepository = $competitionTeamRepository;
    }

    /**
     * @param $team_id
     * @return Nette\Database\Table\Selection
     */
    public function getTeamById($team_id)
    {

        return $this->getTable()->wherePrimary($team_id);
    }

    /**
     * @param $team_number
     * @param $team_letter
     * @return bool|mixed|Nette\Database\Table\IRow
     */
    public function getTeamByName($team_number, $team_letter)
    {
        return $this->getTable()->where('number', $team_number)->where('letter', $team_letter)->fetch();
    }

    /**
     * Gets all teams registered in the system
     *
     * @return Nette\Database\Table\Selection
     */
    public function getAllTeams()
    {
        return $this->getTable()->order('number');
    }

    /**
     * Gets all teams registered by admin group (imported from VexDB)
     *
     * @return Nette\Database\Table\Selection
     */
    public function getPublicTeams()
    {
        return $this->getTable()->where('group_id', $this->settings->__get('admin_group_id'));
    }

    /**
     * Gets all teams add by one particular group
     *
     * @param $group_id
     * @return Nette\Database\Table\Selection
     */
    public function getPrivateTeams($group_id)
    {
        return $this->getTable()->where('group_id', $group_id);
    }

    /**
     * @param $competition_id
     * @return array
     */
    public function getTeamsForCompetition($competition_id)
    {
        return $this->competitionTeamRepository->getRelatedTeams($competition_id);
    }

    /**
     * @param $competition_id
     * @return array [id => team_number.$team_letter]
     */
    public function getTeamsPairs($competition_id)
    {
        $result = array();
        foreach ($this->getTeamsForCompetition($competition_id) as $row) {
            $result[$row->id] = $row->number.$row->letter;
        }

        return $result;
    }

    /**
     * Creates new team
     *
     * @param ArrayHash $values
     * @todo It should test for duplicities
     */
    public function createTeam(ArrayHash $values)
    {
        $this->getTable()->insert($values);
    }

    /**
     * Import teams from Vex DB
     * @param $JSON_file
     * @todo: use api http://api.vex.us.nallen.me/get_teams
     */
    public function importTeamsJSON($JSON_file)
    {

    }
}
