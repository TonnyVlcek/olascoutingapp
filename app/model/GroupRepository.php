<?php
namespace ScoutingOla\Model;

use Nette\Security;
use Nette\ArrayHash;
use Nette;

/**
 * Class GroupRepository
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class GroupRepository extends BaseRepository
{

    /**
     * @param $name
     * @return bool|mixed|Nette\Database\Table\IRow
     */
    public function getGroupByName($name)
    {
       return $this->getTable()->where('name', $name)->fetch();
    }

    /**
     * @param $group_id
     * @return Nette\Database\Table\IRow
     */
    public function getGroupById($group_id)
    {
        return $this->getTable()->get($group_id);
    }

    /**
     * @param $user_id
     * @return bool|mixed|Nette\Database\Table\IRow
     */
    public function getUsersGroup($user_id)
    {
        return $this->getTable()->where('user.id', $user_id)->fetch(); //1:1 - one group per user
    }

    /**
     * Selects all groups in the system
     *
     * @return array|Nette\Database\Table\IRow[]
     */
    public function getGroups()
    {
        return $this->getTable()->order('name')->fetchAll();
    }

    /**
     * @return array [id => name]
     */
    public function getGroupsIdNamePairs()
    {
        return $this->getTable()->fetchPairs('id','name');

    }

    /**
     * Returns current number (int) of members of the group
     *
     * @param $group_id
     * @return int
     */
    public function getNumberOfMembers($group_id)
    {
        $group = $this->getGroupById($group_id);
        return count($group->related("user"));
    }

    /**
     * Creates new group
     * Checks if the name is unique
     *
     * @param ArrayHash $values
     * @return bool|int|Nette\Database\Table\IRow
     * @throws \Nette\Security\AuthenticationException
     */
    public function createGroup(ArrayHash $values)
    {
        if ($this->getGroupByName($values->name)){
            throw new Security\AuthenticationException('Group with name "%s" already exists');
        }

        return $this->getTable()->insert($values);
    }

    /**
     * Edits the group's basic parameters
     *
     * @param $id
     * @param $name
     * @param $description
     * @throws \Nette\Security\AuthenticationException
     * @todo Extend for other parameters
     */
    public function editGroup($id, $name, $description)
    {
        $group = $this->getGroupById($id);

        if(($this->getGroupByName($name))&&($group->name != $name)){
            throw new Security\AuthenticationException('Group with name %s already exists.');
        }

        $group->update(array(
            'name' => $name,
            'description' => $description));
    }
}
