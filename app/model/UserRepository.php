<?php
namespace ScoutingOla\Model;

use Nette;
use Nette\ArrayHash;
use Nette\Security\AuthenticationException;
use ScoutingOla\Security;

/**
 * Class UserRepository
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class UserRepository extends BaseRepository
{
    /** @var GroupRepository */
    public $groupRepository;

    /**
     * @param string $table
     * @param Nette\Database\Context $context
     * @param GroupRepository $groupRepository
     */
    public function __construct($table, Nette\Database\Context $context, GroupRepository $groupRepository)
    {
        parent::__construct($table, $context);
        $this->groupRepository = $groupRepository;
    }

    /**
     * Returns all users sorted by id
     *
     * @return Nette\Database\Table\Selection
     */
    public function getUsers()
    {
        return $this->getTable()->order('id');
    }

    /**
     * Selects user based on his/hers unique email address.
     * Email is not required for all users, therefore this method is not universal.
     *
     * @param $email
     * @return bool|mixed|Nette\Database\Table\IRow
     */
    public function getUserByEmail($email)
    {
        return $this->getTable()->where('email', $email)->limit(1)->fetch();
    }

    /**
     * Selects user based on his/hers unique id.
     * This method is used as universal to get users information.
     *
     * @param $id
     * @return bool|mixed|Nette\Database\Table\IRow
     */
    public function getUserById($id)
    {
        return $this->getTable()->where('id', $id)->limit(1)->fetch();
    }

    /**
     * Gets all members (including admins) of one group based on group id.
     *
     * @param $group_id
     * @return Nette\Database\Table\Selection
     */
    public function getAllGroupMembers($group_id)
    {
        return $this->getTable()->where('group_id', $group_id);
    }

    public function userInGroup($group_id, $user_name)
    {
        return $this->getTable()->where("group_id", $group_id)->where("name", $user_name)->limit(1)->fetch();
    }

    /**
     * Check for email to be unique, name to be unique within a group, and the limit of scouts not to be exceeded.
     *
     * @param $name
     * @param $email
     * @param Nette\Database\Table\IRow $group
     * @return bool
     * @throws \Nette\Security\AuthenticationException
     */
    public function userCheckConditions($name, $email, Nette\Database\Table\IRow $group)
    {
        //unique email in the whole application
        if($this->getUserByEmail($email)){
            throw new AuthenticationException("User with this email already exists.");
        }

        //unique name in a group
        if($this->getTable()->where('group_id', $group->id)->where('name', $name)->fetch()){
            throw new AuthenticationException("User with this name already exists in this group.");
        }

        //can not go over scout limit
        if($group->scout_limit <= count($group->related("user"))) {
            throw new AuthenticationException("You have reached your scout limit.");
        }

        return true;
    }

    /**
     * Creates new user.
     *
     * @param ArrayHash $values
     * @return bool|int|Nette\Database\Table\IRow
     * @throws \Nette\Security\AuthenticationException
     */
    public function newUser (ArrayHash $values)
    {
       $group = $this->groupRepository->getGroupById($values->group_id);

       try {
           $this->userCheckConditions($values->name, $values->email, $group);
       } catch (AuthenticationException $e) {
           throw new AuthenticationException($e->getMessage());
       }

       $values->password = md5($values->password);
       return $this->getTable()->insert($values);
    }

    /**
     * Changes values for a user.
     *
     * @param $id
     * @param ArrayHash $values
     * @return mixed
     * @throws \Nette\Security\AuthenticationException
     */
    public function editUser($id, ArrayHash $values)
    {
        $user = $this->getUserById($id);
        //$group = $this->groupRepository->getGroupById($user->group_id); {TODO: check for conditions}

        return $user->update($values);
    }

    /**
     * Deletes user based on his/hers id.
     *
     * @param $user_id
     * @throws \Exception
     * @todo check for what happens with dependencies in database
     */
    public function deleteUser($user_id)
    {
        $user = $this->getUserById($user_id);
        if($user->role === 'admin'){
            throw new \Exception('Admin user can\'t be deleted');
        }
        $user->delete();
    }
}
