<?php
namespace ScoutingOla\Model;

use Nette;

/**
 * Class CompetitionTeamRepository
 * Stands for M:N connection between competitions and related teams.
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class CompetitionTeamRepository extends BaseRepository
{
    /**
     * Gets all related teams for the competition
     *
     * @param $competition_id
     * @return array
     */
    public function getRelatedTeams($competition_id)
    {
        $connections = $this->getTable()->where('competition_id', $competition_id);
        $teams = array();
        foreach($connections as $connection) {
            $teams[] = $connection->ref('team','team_id');
        }

        return $teams;
    }

    /**
     * Adds line (connection) to the linking table
     *
     * @param $competition_id
     * @param $teams
     */
    public function addConnection($competition_id, $teams)
    {
        foreach($teams as $team){
            $this->getTable()->insert(array("competition_id" => $competition_id, "team_id" => $team));
        }
    }
} 