<?php
namespace ScoutingOla\Model;

use Nette;

/**
 * Class RecordTagRepository
 * Stands for M:N connection between records and related tags.
 *
 * @package ScoutingOla\Model
 * @author Tony Vlcek
 */
class RecordTagRepository extends BaseRepository
{

    /**
     * Gets all related tag for the record
     *
     * @param $record_id
     * @return array
     */
    public function getRelatedTags($record_id)
    {
        $connections = $this->getTable()->where('record_id', $record_id);
        $tags = array();
        foreach($connections as $connection) {
            $tags[] = $connection->ref('tag','tag_id');
        }

        return $tags;
    }

    /**
     * Adds line (connection) to the linking table
     *
     * @param $record_id
     * @param $tags
     */
    public function addConnection($record_id, $tags)
    {
        foreach($tags as $tag){
            $this->getTable()->insert(array("record_id" => $record_id, "tag_id" => $tag));
        }
    }
}
